class BorrowingsController < ApplicationController
	before_action :logged_in_user
  before_action :librarian_user, only: [:approve, :deny_borrow_request, :deny_renew_request, :return_book]

  def index
    if !current_user.librarian?
      @borrowings = current_user.borrowings.paginate(:page => params[:page])
    else
      @borrowings = Borrowing.requesting.paginate(:page => params[:page])
    end
  end

  def borrowing_all
    if current_user.librarian?
      @borrowings = Borrowing.borrowed.paginate(:page => params[:page])
    end
  end

	def create #borrow
    @book = Book.find(params[:book_id])
    if @book.available?
      if current_user.borrowing.count == 5
        flash[:danger] = "Your borrowings is limited. You cann't borrow over 5 books!"
        redirect_to @book
      else
        current_user.borrow(@book)
        respond_to do |format|
        format.html { redirect_to @book }
        format.js
        end
      end
    else
      flash[:danger] = "Sorry! Book is limited!"
      redirect_to @book
    end
	end

  def update #renew request
    @borrowing = Borrowing.find(params[:id])
    if @borrowing.status == 0
      flash[:danger] = "#{@borrowing.book.title} have not approved!"
    else
      if @borrowing.renew_num >= 3
        flash[:danger] = "You cannot renew #{@borrowing.book.title}"
      else
        @borrowing.update_attribute(:renew_num, @borrowing.renew_num + 1)
        @borrowing.update_attribute(:status, 2)
        flash[:success] = "Renew request successfull #{@borrowing.book.title} have to wait to approve!"
      end
    end
    redirect_to borrowings_path
  end

  def deny_renew_request
    if @borrowing = Borrowing.find(params[:borrowing_id])
      @borrowing.update_attribute(:renew_num, @borrowing.renew_num - 1)
      @borrowing.update_attribute(:status, 1)
    else
      flash[:danger] = "Error. Try again!"
    end
    redirect_to borrowings_path
  end

  def approve # for borrow
    if @borrowing = Borrowing.find(params[:borrowing_id])
      @borrowing.update_attribute(:status, 1)
      @borrowing.update_attribute(:renew_at, Time.zone.now)
    else
      flash[:danger] = "Error. Try again!"
    end
    redirect_to borrowings_path
  end

  def deny_borrow_request
    if @borrowing = Borrowing.find(params[:borrowing_id])
      @borrowing.user.unborrow(@borrowing.book)
    else
      flash[:danger] = "Error. Try again!"
    end
    redirect_to borrowings_path
  end


  def destroy 
    borrowing = Borrowing.find(params[:id])
    borrowing.user.unborrow(borrowing.book)
    flash[:success] = "Book deleted"
    redirect_back_or(return_books_path) 
  end

  def return_book
    if params[:user_id].present?
      @user = User.find(params[:user_id])
      @borrowings = @user.borrowings.paginate(:page => params[:page])
    end
  end

  private
    def librarian_user
      if current_user.nil?
        redirect_to (root_url)
      else
        redirect_to (root_url) unless current_user.librarian?
      end
    end

end
