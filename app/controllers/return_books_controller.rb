class ReturnBooksController < ApplicationController

	def index
		if params[:user_id].present?
			store_location
      @user = User.find(params[:user_id])
      @return_books = @user.borrowings.paginate(:page => params[:page])
    end
	end
end
