require 'will_paginate/array'

class SearchController < ApplicationController

	def search
		@search = []
		params.slice(:title, :author, :dewey_decimal)
		# if params[:title].present? || params[:author].present? || params[:dewey_decimal].present?
			
			if params[:dewey_decimal].present?													
				catalogs = Catalog.where(["dewey_decimal = ?", params[:dewey_decimal]])																												 
				if !catalogs.nil?
					catalogs.each do |catalog|
						catalog.books.title(params[:title]).author(params[:author]).each do |book|
							@search.push book
						end
					end
				end
			else 
				@search = Book.title(params[:title]).author(params[:author])
			end
		# end
		@search = @search.paginate(:page => params[:page], :per_page => 14)
		respond_to do |format|
		    format.html # search.html.erb
		    format.js
		end
	end

end
