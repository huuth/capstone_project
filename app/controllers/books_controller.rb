class BooksController < ApplicationController

	before_action :librarian_user, only: [:new, :create, :update, :destroy, :edit]

	def new
		@catalogs = Catalog.all
		@book = Book.new
	end

	def show
		@book = Book.find(params[:id])
	end

	def index
		@books = current_user.borrowing.paginate(:page => params[:page])
	end

	def create
		params[:book][:availability] = params[:book][:quantity]
		@book = current_user.books.build(book_params)
		if @book.save
			flash[:success] = "Book created!"
			redirect_to @book
		else
			@catalogs = Catalog.all
			render 'new'
		end
	end

	def edit
		@catalogs = Catalog.all
		@book = Book.find(params[:id])
	end

	def update
		@book = Book.find(params[:id])
		@book[:availability] -= @book[:quantity].to_i - params[:book][:quantity].to_i

		if @book.update_attributes(book_params)
			flash['success'] = "Book updated!"
			redirect_to @book
		else
			render 'edit'
		end
	end

	def destroy
		Book.find(params[:id]).destroy
		flash[:success] = "Book deleted"
		redirect_to request.referrer || root_url
	end

	private

		def book_params
			params.require(:book).permit(:title, :publisher, :year, :quantity, :author, :catalog_id, :dewey_id, :availability)
		end

		def librarian_user
			if current_user.nil?
				redirect_to (login_url)
			else
				redirect_to (login_url) unless current_user.librarian?
			end
		end

end
