class User < ApplicationRecord

	has_many :books
	validates :name, presence: true, length: {maximum: 50}
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  	validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }, allow_nil: true
	validates :name, presence: true, length: {maximum: 50}

	attr_accessor :remember_token, :activation_token, :reset_token
  	before_save   :downcase_email
  	before_create :create_activation_digest


  	has_many :borrowings, class_name:  "Borrowing",
                                  foreign_key: "user_id",
                                  dependent:   :destroy

    has_many :borrowing, through: :borrowings, source: :book




  	def User.digest(string)
    	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  	  BCrypt::Engine.cost
    	BCrypt::Password.create(string, cost: cost)
	end

	def User.new_token
		SecureRandom.urlsafe_base64
	end

	def send_activation_email
		UserMailer.account_activation(self).deliver_now
	end

	def activate
		update_attribute(:activated, true)
		update_attribute(:activated_at, Time.zone.now)
	end

	def create_reset_digest
		self.reset_token = User.new_token
		update_attribute(:reset_digest, User.digest(reset_token))
		update_attribute(:reset_sent_at, Time.zone.now)
	end

	def send_password_reset_email
		UserMailer.password_reset(self).deliver_now
	end

	# Returns true if the given token matches the digest.
	def authenticated?(attribute, token)
	  	digest = send("#{attribute}_digest")
	  	return false if digest.nil?
	  	BCrypt::Password.new(digest).is_password?(token)
	end

	def borrow(book)
		# borrowing << book
		Borrowing.create!(user_id: self.id,
						  book_id: book.id, 
						  request_at: Time.zone.now,
						  renew_at: Time.zone.now,
						  status: 0)
		book.update_attribute(:availability, book.availability - 1)
	end

	def unborrow(book)
		borrowing.delete(book)
		book.update_attribute(:availability, book.availability + 1)
	end

	def borrowing?(book)
  		borrowing.include?(book)
  	end

  	private

	    # Converts email to all lower-case.
	    def downcase_email
	      self.email = email.downcase
	    end

	    # Creates and assigns the activation token and digest.
	    def create_activation_digest
	      self.activation_token  = User.new_token
	      self.activation_digest = User.digest(activation_token)
	    end
end
