class Borrowing < ApplicationRecord
  belongs_to :user, class_name: "User"
  belongs_to :book, class_name: "Book"

  def self.requesting
  	Borrowing.where(["status != ?", 1])
  end

  def self.borrowed
    Borrowing.where(["status == ? or status == ?", 1, 2])
  end

  def time_remaining
  	time_remaining = self.book.maximum_days - (Time.zone.now.to_date  - self.renew_at.to_date).to_i
  	if time_remaining < 0
  		time_remaining = 0
  	end
  	time_remaining
  end

end
