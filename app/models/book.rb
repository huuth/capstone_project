class Book < ApplicationRecord
  belongs_to :user
  belongs_to :catalog
  validates :user_id, presence: true
  validates :title, presence: true
  validates :quantity, numericality: { only_integer: true, greater_than: -1 }

  scope :title, -> (title) { where("title like ?", "%#{title}%")}
  scope :author, -> (author) { where("author like ?", "%#{author}%")}

  def available?
  	if self.availability > 0
  		return true
  	else
  		return false
  	end
  end

end
