Rails.application.routes.draw do
  resources :password_resets,     only: [:new, :create, :edit, :update]

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  root 'static_pages#home'

  get '/help', to: 'static_pages#help'
  get '/home', to: 'static_pages#home'

  get '/signup', to: 'users#new'
  resources :users
  post '/signup', to: 'users#create'
  resources :account_activations, only: [:edit]

  resources :books

  resources :borrowings,       only: [:index, :create, :destroy, :update]
  get '/borrowing/all', to: "borrowings#borrowing_all"
  patch '/approve', to: "borrowings#approve"
  patch '/deny_renew', to: "borrowings#deny_renew_request"
  patch '/deny_borrow', to: "borrowings#deny_borrow_request"
  get '/return', to: "borrowings#return_book"

  get '/search', to: 'search#search'


  resources :return_books, only: [:index]

end
