# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


user1 = User.create!(name:  "Thanh Huu",
             email: "truongthanhhhuu@gmail.com",
             password:              "121212",
             password_confirmation: "121212",
             librarian:     true,
             activated: true,
             activated_at: Time.zone.now)

user2 = User.create!(name:  "Example Librarian",
             email: "example@railstutorial.org",
             password:              "121212",
             password_confirmation: "121212",
             librarian:     true,
             activated: true,
             activated_at: Time.zone.now)

user3 = User.create!(name:  "Example Librarian",
             email: "user1@railstutorial.org",
             password:              "121212",
             password_confirmation: "121212",
             librarian:     false,
             activated: true,
             activated_at: Time.zone.now)

user4 = User.create!(name:  "Example Librarian",
             email: "user2@railstutorial.org",
             password:              "121212",
             password_confirmation: "121212",
             librarian:     false,
             activated: true,
             activated_at: Time.zone.now)


5.times do |n|
  	name  = Faker::Name.name
  	email = "example-#{n+1}@railstutorial.org"
  	password = "password"
  	User.create!(name:  name,
              	email: email,
              	password:              password,
              	password_confirmation: password,
              	activated: true,
              	activated_at: Time.zone.now)
end


10.times do |n|
  dewey_decimal = Faker::Number.unique.between(0, 999)
  3.times do
  	name = Faker::Book.unique.genre
    characteristic = Faker::Number.unique.between(10, 399)
  	Catalog.create!(name: name, dewey_decimal: dewey_decimal, identifier: characteristic)
  end
end

prng = Random.new

30.times do	
  	title  = Faker::Book.title
  	author = Faker::Book.author
  	quantity = Faker::Number.between(1, 20)
  	availability = quantity - prng.rand(quantity)
  	catalog_id = prng.rand(1..30)
  	year = Faker::Number.between(1995, 2015)
    publisher = Faker::Book.publisher
  	maximum_days = Faker::Number.between(14, 28)
  	user1.books.create!(title: title,
  						author: author,
  						quantity: quantity,
  						availability: availability,
  						catalog_id: catalog_id,
              publisher: publisher,
  						year: year,
              maximum_days: maximum_days)
end

30.times do	
  	title  = Faker::Book.title
  	author = Faker::Book.author
  	quantity = Faker::Number.between(1, 20)
  	availability = quantity - prng.rand(quantity)
    publisher = Faker::Book.publisher
  	catalog_id = prng.rand(1..30)
  	year = Faker::Number.between(1995, 2015)
  	maximum_days = Faker::Number.between(14, 28)
  	user2.books.create!(title: title,
  						author: author,
  						quantity: quantity,
  						availability: availability,
  						catalog_id: catalog_id,
              publisher: publisher,
  						year: year,
              maximum_days: maximum_days)
end



books = Book.all
borrow1 = books[1..3]
borrow1.each do |book|
  user3.borrow(book)
end
