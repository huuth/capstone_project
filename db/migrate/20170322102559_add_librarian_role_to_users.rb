class AddLibrarianRoleToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :librarian, :boolean, default: false
  end
end
