class RenameCharacteristicInTableCatalogsToIdentifier < ActiveRecord::Migration[5.0]
  def change
  	rename_column :catalogs, :characteristic, :identifier
  end
end
