class RemoveDeweyIdFromBooks < ActiveRecord::Migration[5.0]
  def change
  	remove_column :books, :dewey_id, :string
  end
end
