class AddMaximumDaysToBooks < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :maximum_days, :integer
  end
end
