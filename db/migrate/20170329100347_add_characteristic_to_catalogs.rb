class AddCharacteristicToCatalogs < ActiveRecord::Migration[5.0]
  def change
    add_column :catalogs, :characteristic, :integer
  end
end
