class AddDeweyDecimalToCatalog < ActiveRecord::Migration[5.0]
  def change
    add_column :catalogs, :dewey_decimal, :integer, :unique => true
  end
end
