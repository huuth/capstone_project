class CreateBorrowings < ActiveRecord::Migration[5.0]
  CREATE_TIMESTAMP = 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP'

  def change
    create_table :borrowings do |t|
      t.references :user, foreign_key: true
      t.references :book, foreign_key: true
      t.integer :status, default: 0
      t.datetime :request_at, default: Time.now
      t.datetime :approve_at
      t.datetime :renew_at, default: Time.now
      t.integer :renew_num, default: 0

      t.timestamps
    end
  end
end
