class AddRefToBooks < ActiveRecord::Migration[5.0]
  def change
    add_reference :books, :catalog, foreign_key: true
  end
end
