require 'test_helper'

class BorrowingTest < ActiveSupport::TestCase
  
  def setup
		@borrowing = Borrowing.new(user_id: users(:archer).id,
															book_id: books(:zero).id)
	end

	test "borrowing should be valid" do
		assert @borrowing.valid?
	end

	test "borrowing require a user id" do
		@borrowing.user_id = nil
		assert_not @borrowing.valid?
	end

	test "borrowing require a book id" do
		@borrowing.book_id = nil
		assert_not @borrowing.valid?
	end
end
