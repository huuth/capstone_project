require 'test_helper'

class BookTest < ActiveSupport::TestCase
  def setup
  	@user = users(:michael)
  	@catalog = catalogs(:cat)

  	@book = @user.books.build(title: "title",
  						author: "author",
  						quantity: 10,
  						catalog_id: @catalog.id)
  end
  
  test "book should be valid" do
  	assert @book.valid?
  end

  test "book quantity should be invalid" do
  	@book.quantity = -1
  	assert_not @book.valid?
  end

  test "book title should be invalid" do
  	@book.title = ""
  	assert_not @book.valid?
  end

  test "book user_id should be invalid" do
  	@book.user_id = nil
  	assert_not @book.valid?
  end
end
