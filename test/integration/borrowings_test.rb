require 'test_helper'

class BorrowingsTest < ActionDispatch::IntegrationTest
  
  def setup
  	@librarian = users(:michael)
  	@user = users(:qtv)
  	@book = books(:one)
    @user_renew_maximum = users(:user_renew_maximum)
    @borrowing_requesting = borrowings(:one)
    @borrowing_approved = borrowings(:two)
    @borrowing_renew = borrowings(:borrowing_renew)
    @renew_maximum = borrowings(:three)
    @user_max_borrowing = users(:user_max_borrowing)
  end

  test "borrowing request interface for user successful" do
  	log_in_as(@user)
  	get book_path(@book)
  	assert_template 'books/show'
  	assert_select "div#borrow_form"
    assert_difference 'Borrowing.count', 1 do
  	  post borrowings_path, params: {book_id: @book.id}
  	end
    get book_path(@book)
  	assert_select "p", 'Requested'
    delete logout_path
    log_in_as(users(:librarian))
    get borrowings_path
    assert_template 'borrowings/index'
    assert_select "a[href=?]", book_path(@book)
    assert_select "a[href=?]", user_path(@user)
    assert_select "form[action=?]", approve_path
    assert_select "form[action=?]", deny_borrow_path
    assert_select "span#requesting"    
  end

  test "borrowing approve request" do
    log_in_as(@librarian)
    get borrowings_path
    assert_template 'borrowings/index'
    assert_select 'a[href=?]', book_path(@borrowing_requesting.book)
    assert_select "a[href=?]", user_path(@borrowing_requesting.user)
    assert_select "form[action=?]", approve_path
    assert_select "form[action=?]", deny_borrow_path
    patch approve_path, params: {borrowing_id: @borrowing_requesting.id}
    assert_select 'a[href=?]', book_path(@borrowing_requesting.book), false
  end

  test "borrowing deny request" do
    log_in_as(@librarian)
    get borrowings_path
    assert_template 'borrowings/index'
    assert_select 'a[href=?]', book_path(@borrowing_requesting.book)
    assert_select "a[href=?]", user_path(@borrowing_requesting.user)
    assert_select "form[action=?]", approve_path
    assert_select "form[action=?]", deny_borrow_path
    patch deny_borrow_path, params: {borrowing_id: @borrowing_requesting.id}
    assert_select 'a[href=?]', book_path(@borrowing_requesting.book), false
  end

  test "renew request" do
    log_in_as @user
    get borrowings_path
    assert_template 'borrowings/index'
    assert_select 'a[href=?]', book_path(@borrowing_approved.book)
    assert_select 'form[action=?]', borrowing_path(@borrowing_approved)
    assert_select 'input.renew-btn'
    patch borrowing_path(@borrowing_approved)
    follow_redirect!
    assert_select 'input.renew-btn[disabled="disabled"]'
    assert_not flash.empty?
  end

  test "approve renew request" do
    log_in_as @librarian
    get borrowings_path
    assert_template 'borrowings/index'
    assert_select 'a[href=?]', book_path(@borrowing_renew.book)
    assert_select "a[href=?]", user_path(@borrowing_renew.user)
    assert_select "form[action=?]", approve_path
    assert_select "form[action=?]", deny_renew_path
    assert_no_difference '@borrowing_renew.renew_num' do
      patch deny_renew_path, params: {borrowing_id: @borrowing_renew.id}
    end
    follow_redirect!
    assert_select 'a[href=?]', book_path(@borrowing_renew.book), false
  end

  test "deny renew request" do
    log_in_as @librarian
    get borrowings_path
    assert_template 'borrowings/index'
    assert_select 'a[href=?]', book_path(@borrowing_renew.book)
    assert_select "a[href=?]", user_path(@borrowing_renew.user)
    
    assert_select "form[action=?]", approve_path
    assert_select "form[action=?]", deny_renew_path

    patch deny_renew_path, params: {borrowing_id: @borrowing_renew.id}
    follow_redirect!
    assert_select 'a[href=?]', book_path(@borrowing_renew.book), false
  end

  test "unable to renew request with 3 times maximum" do
    log_in_as @user
    get borrowings_path
    assert_template 'borrowings/index'
    # assert_select 'a[href=?]', book_path(@renew_maximum.book)
    # assert_select 'form[action=?]', borrowing_path(@renew_maximum)
    assert_select 'input.renew-btn'
    assert_no_difference ['@renew_maximum.renew_num', '@renew_maximum.status'] do
      patch borrowing_path(@renew_maximum)
    end
    follow_redirect!
    assert_not flash.empty?
  end

  test 'unable to borrowing request by 5 times maximum' do
    log_in_as @user_max_borrowing
    assert @user_max_borrowing.borrowing.count, 5
    get book_path(@book)
    assert_template 'books/show'
    assert_select "div#borrow_form"
    assert_difference 'Borrowing.count', 1 do
      post borrowings_path, params: {book_id: @book.id}
    end
    follow_redirect!
    assert_select "p", 'Requested'
  end

  test "borrowing interface for librarian" do
  	log_in_as(@librarian)
  	get book_path(@book)
  	assert_template 'books/show'
  	assert_select "a[href=?]", edit_book_path
  end

  test "borrowing interface not log in" do
  	get book_path(@book)
  	assert_template 'books/show'
  	assert_select "a[href=?]", edit_book_path, false
  	assert_select "a[href=?]", edit_book_path, false
  end
end
