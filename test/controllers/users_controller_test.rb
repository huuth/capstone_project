require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
  	@librarian = users(:michael)
  	@other_user = users(:archer)
  end

  test "should get new" do
		get signup_path
		assert_response :success	
	end

	test "should redirect edit when not logged in" do
		get edit_user_path(@librarian)
		assert_not flash.empty?
		assert_redirected_to login_url
	end

	test "should redirect update when not logged in" do
		patch user_path(@librarian), params: { user: { name: @librarian.name,
                                              email: @librarian.email } }
    assert_not flash.empty?
    assert_redirected_to login_url
	end
	
end
