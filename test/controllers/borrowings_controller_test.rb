require 'test_helper'

class BorrowingsControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:qtv)
    @librarian = users(:michael)
    @book = books(:one)
  end

	test "create should require logged-in user" do
    assert_no_difference 'Borrowing.count' do
      post borrowings_path
    end
    assert_redirected_to login_url
  end

  test "destroy should require logged-in user" do
    assert_no_difference 'Borrowing.count' do
      delete borrowing_path(books(:one))
    end
    assert_redirected_to login_url
  end

  test "approve should require log in as librarian" do
    log_in_as(users(:qtv))
    patch approve_path
    assert_redirected_to root_url
  end

  test "deny renew should require log in as librarian" do
    log_in_as(users(:qtv))
    patch deny_renew_path
    assert_redirected_to root_url
  end

  test "deny borrow should require log in as librarian" do
    log_in_as(users(:qtv))
    patch deny_borrow_path
    assert_redirected_to root_url
  end

  test "return book should require log in as librarian" do
    log_in_as(users(:qtv))
    get return_path
    assert_redirected_to root_url
  end

  test "test valid borrowing request" do
    log_in_as(@user)
    assert_difference "Borrowing.count", 1 do
      post borrowings_path, params: {book_id: @book.id}
    end
  end

  test "test invalid borrowing request not log in" do
    assert_no_difference "Borrowing.count", 1 do
      post borrowings_path, params: {book_id: @book.id}
    end
  end
  
  test "test invalid borrowing request maximum 5 request" do
    log_in_as(@user)
    post borrowings_path, params: {book_id: books(:book_1).id}
    post borrowings_path, params: {book_id: books(:book_2).id}
    post borrowings_path, params: {book_id: books(:book_3).id}
    post borrowings_path, params: {book_id: books(:book_4).id}
    post borrowings_path, params: {book_id: books(:book_5).id}
    assert_no_difference "Borrowing.count" do
      post borrowings_path, params: {book_id: @book.id}
    end
  end

  # test "test deny request require librarian" do
  #   log_in_as(@user)
  #   post borrowings_path, params: {book_id: books(:book_1).id}
  #   log_in_as(@librarian)
    
  # end

end
