require 'test_helper'

class BooksControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @book = books(:zero)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Book.count' do
      post books_path, params: { book: { title: "Lorem ipsum" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Book.count' do
      delete book_path(@book)
    end
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch book_path(@book), params: { book: { title: "update title" } }
    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong book" do
    log_in_as(users(:archer))
    assert_no_difference 'Book.count' do
      delete book_path(@book)
    end
    assert_redirected_to login_url
  end

end
